const jwt = require("jsonwebtoken");
const db = require("../db");
const dotenv = require("dotenv");
const Auth = {
  /**
   * Verify Token
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object|void} response object
   */
  async verifyToken(req, res, next) {
    const token = req.headers["access-token"];
    if (!token) {
      return res.status(400).send({ message: "Token não fornecido." });
    }
    try {
      const decoded = await jwt.verify(token, process.env.SECRET);
      const text = "SELECT * FROM users WHERE id = $1";
      const { rows } = await db.query(text, [decoded.userId]);
      if (!rows[0]) {
        return res.status(400).send({ message: "Token inválida." });
      }
      req.user = { id: decoded.userId };
      next();
    } catch (error) {
      return res.status(400).send(error);
    }
  }
};

module.exports = Auth;
