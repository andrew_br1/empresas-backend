class User {
  /**
   * class constructor
   * @param {object} data
   */
  constructor() {
    this.users = [];
  }
  /**
   * 
   * @returns {object} user object
   */
  create(data) {
    const newuser = {
      
      sucess: data.success || '',
      lowPoint: data.lowPoint || '',
      takeAway: data.takeAway || '',
      
    };
    this.users.push(newuser);
    return newuser
  }
  /**
   * 
   * @param {id} id
   * @returns {object} user object
   */
  findById(id) {
    return this.users.find(reflect => reflect.id === id);
  }
  /**
   * @returns {object} returns all users
   */
  findAll() {
    return this.users;
  }
  /**
   * 
   * @param {id} id
   * @param {object} data 
   */
  update(id, data) {
    const user = this.findById(id);
    const index = this.users.indexOf(user);
    this.users[index].success = data['success'] || user.success;
    this.users[index].lowPoint = data['lowPoint'] || user.lowPoint;
    this.users[index].takeAway = data['takeAway'] || user.takeAway;
    this.users[index].modifiedDate = moment.now()
    return this.users[index];
  }
  /**
   * 
   * @param {id} id 
   */
  delete(id) {
    const user = this.findById(id);
    const index = this.users.indexOf(user);
    this.users.splice(index, 1);
    return {};
  }
}
module.exports = new User();