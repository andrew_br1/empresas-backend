class Portfolio {
    /**
     * class constructor
     * @param {object} data
     */
    constructor() {
      this.portfolios = [];
    }
    /**
     * 
     * @returns {object} portfolio object
     */
    create(data) {
      const newportfolio = {
        
        investor: data.success || '',
        lowPoint: data.lowPoint || '',
        takeAway: data.takeAway || '',
        
      };
      this.portfolios.push(newportfolio);
      return newportfolio
    }
    /**
     * 
     * @param {id} id
     * @returns {object} portfolio object
     */
    findById(id) {
      return this.portfolios.find(reflect => reflect.id === id);
    }
    /**
     * @returns {object} returns all portfolios
     */
    findAll() {
      return this.portfolios;
    }
    /**
     * 
     * @param {id} id
     * @param {object} data 
     */
    update(id, data) {
      const portfolio = this.findById(id);
      const index = this.portfolios.indexOf(portfolio);
      this.portfolios[index].success = data['success'] || portfolio.success;
      this.portfolios[index].lowPoint = data['lowPoint'] || portfolio.lowPoint;
      this.portfolios[index].takeAway = data['takeAway'] || portfolio.takeAway;
      this.portfolios[index].modifiedDate = moment.now()
      return this.portfolios[index];
    }
    /**
     * 
     * @param {id} id 
     */
    delete(id) {
      const portfolio = this.findById(id);
      const index = this.portfolios.indexOf(portfolio);
      this.portfolios.splice(index, 1);
      return {};
    }
  }
  module.exports = new Portfolio();