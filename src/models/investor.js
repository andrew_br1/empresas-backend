class Investor {
    /**
     * class constructor
     * @param {object} data
     */
    constructor() {
      this.investors = [];
    }
    /**
     * 
     * @returns {object} investor object
     */
    create(data) {
      const newinvestor = {
        
        investor: data.success || '',
        lowPoint: data.lowPoint || '',
        takeAway: data.takeAway || '',
        
      };
      this.investors.push(newinvestor);
      return newinvestor
    }
    /**
     * 
     * @param {id} id
     * @returns {object} investor object
     */
    findById(id) {
      return this.investors.find(reflect => reflect.id === id);
    }
    /**
     * @returns {object} returns all investors
     */
    findAll() {
      return this.investors;
    }
    /**
     * 
     * @param {id} id
     * @param {object} data 
     */
    update(id, data) {
      const investor = this.findById(id);
      const index = this.investors.indexOf(investor);
      this.investors[index].success = data['success'] || investor.success;
      this.investors[index].lowPoint = data['lowPoint'] || investor.lowPoint;
      this.investors[index].takeAway = data['takeAway'] || investor.takeAway;
      this.investors[index].modifiedDate = moment.now()
      return this.investors[index];
    }
    /**
     * 
     * @param {id} id 
     */
    delete(id) {
      const investor = this.findById(id);
      const index = this.investors.indexOf(investor);
      this.investors.splice(index, 1);
      return {};
    }
  }
  module.exports = new Investor();