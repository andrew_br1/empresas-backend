const { Pool } = require("pg");
const dotenv = require("dotenv");

dotenv.config();

const pool = new Pool({
  connectionString: process.env.DATABASE_URL
});

pool.on("connect", () => {
  console.log("connected to the db");
});

/**
 * Create Tables
 */
const createTables = () => {
  const queryText = `
CREATE TABLE IF NOT EXISTS
Users(
  id SERIAL PRIMARY KEY,
  email VARCHAR(128) unique ,
  password VARCHAR(128) NOT NULL,
  created_date TIMESTAMP,
  modified_date TIMESTAMP
  
);

CREATE TABLE IF NOT EXISTS
Investors(
  id SERIAL,
  investor_name VARCHAR(128) NOT NULL,
  email VARCHAR(128) unique ,
  city VARCHAR(3),
  balance bigint,
  super_angel BOOLEAN,
  portfolio_value bigint,
  first_access BOOLEAN,
  photo VARCHAR(128),
  country VARCHAR(128),
  created_date TIMESTAMP,
  modified_date TIMESTAMP,
  PRIMARY KEY (id),
  user_id integer NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE  
  
);
CREATE TABLE IF NOT EXISTS
Portfolios(
  id SERIAL,
  investor_id integer NOT NULL,
  name VARCHAR(128) NOT NULL,
  created_date TIMESTAMP,
  deleted_date TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (investor_id) REFERENCES investors (id) ON DELETE CASCADE  
);

`;

  // ALTER TABLE investors
  // ADD CONSTRAINT fk_user_id FOREIGN KEY (id) REFERENCES users (id);

  // ALTER TABLE investors
  // ADD CONSTRAINT fk_portfolio_id FOREIGN KEY (id) REFERENCES portfolios (id);

  pool
    .query(queryText)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

/**
 * Drop Tables
 */
const dropTables = () => {
  const queryText =
    "DROP TABLE IF EXISTS users;DROP TABLE IF EXISTS investors;DROP TABLE IF EXISTS portfolios";
  pool
    .query(queryText)
    .then(res => {
      console.log(res);
      pool.end();
    })
    .catch(err => {
      console.log(err);
      pool.end();
    });
};

pool.on("remove", () => {
  console.log("client removed");
  process.exit(0);
});

module.exports = {
  createTables,
  dropTables
};

require("make-runnable");
