const express = require("express");
const User = require("../controllers/user_controller");
const Investor = require("../controllers/investor_controller");
const Portfolio = require("../controllers/portfolio_controller");
// Set up the express app
const app = express();

app.use(express.json());

app.listen(3000);

console.log("app running on port ", 3000);

//AUTH

app.post("/api/v1/users/auth/sign_in", (req, res) => {
  User.login(req, res);
});



// USERS

// Create an user
app.post("/api/v1/users/auth/sign_up", (req, res) => {
  User.create(req, res);
  //return res.status(200).send({ message: "Usuário criado com sucesso." });
});

app.get("/api/v1/users", (req, res) => {
  User.getAll(req, res);
  //return res.status(200).send({'message': 'YAY! Congratulations! Your first endpoint is working'});
});
app.get("/api/v1/users/:id", (req, res) => {
  User.getOne(req, res);
});
app.put("/api/v1/users/:id", (req, res) => {
  User.update(req, res);
});
app.delete("/api/v1/users/:id", (req, res) => {
  User.delete(req, res);
});

// Investor

// Listagem de Empresas DENTRO do portfolio dos investidores
app.get("/api/v1/investors", (req, res) => {
  Investor.getAll(req, res);
  //return res.status(200).send({'message': 'YAY! Congratulations! Your first endpoint is working'});
});
// Listagem de Empresas DENTRO do portfolio de um investidor
app.get("/api/v1/investors/:id", (req, res) => {
  Investor.getOne(req, res);
});
app.put("/api/v1/investors/:id", (req, res) => {
  Investor.update(req, res);
});
app.delete("/api/v1/investors/:id", (req, res) => {
  Investor.delete(req, res);
});

// Portfolio
// Create an portfolio
app.post("/api/v1/portfolios", (req, res) => {
  Portfolio.create(req, res);
  //return res.status(200).send({'message': 'Investidor criado com sucesso.'})
});

app.get("/api/v1/portfolios", (req, res) => {
  Portfolio.getAll(req, res);
  //return res.status(200).send({'message': 'YAY! Congratulations! Your first endpoint is working'});
});
app.get("/api/v1/portfolios/:id", (req, res) => {
  Portfolio.getOne(req, res);
});

app.delete("/api/v1/portfolios/:id", (req, res) => {
  Portfolio.delete(req, res);
});
