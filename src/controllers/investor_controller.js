const db = require("../db/index");
const moment = require("moment");
const Portfolio = require("../controllers/portfolio_controller");
const Investor = {
  /**
   * search A investor
   * @param {object} req
   * @param {object} res
   * @returns {object} investor object
   */
  async search(user) {
    const query1 = `SELECT  
        id,
        investor_name,
        email,
        city,
        country,
        balance,
        photo,
        portfolio_value,
        first_access,
        super_angel FROM investors WHERE user_id = $1`;
    const value1 = [user.id];
    try {
      const { rows } = await db.query(query1, value1);
      const { investor } = Object.create({ investor: rows[0] });
      var result = await Portfolio.search(rows[0]);

      investor.portfolio = result;

      return investor;
    } catch (error) {}
  },
  /**
   * Create A investor
   * @param {object} req
   * @param {object} res
   * @returns {object} investor object
   */
  async create(req, res, user) {
    const userLocal = user;
    const text = `INSERT INTO
      investors(investor_name, email, city,country,balance,photo, created_date, modified_date,user_id,portfolio_value,first_access,super_angel)
      VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
      returning *`;
    const values = [
      req.body.name,
      req.body.email,
      req.body.city,
      req.body.country,
      req.body.balance,
      req.body.photo,
      moment(new Date()),
      moment(new Date()),
      userLocal.id,
      req.body.balance,
      false,
      false
    ];

    try {
      const { rows } = await db.query(text, values);
      return Object.create({ investor: rows[0] });
    } catch (error) {
      //return res.status(400).send("Erro ao criar perfil de investidor.");
    }
  },
  /**
   * Get All investor
   * @param {object} req
   * @param {object} res
   * @returns {object} investors array
   */
  async getAll(req, res) {
    const findAllQuery = "SELECT * FROM investors";
    try {
      const { rows, rowCount } = await db.query(findAllQuery);
      return res.status(200).send({ rows, rowCount });
    } catch (error) {
      return res.status(400).send(error);
    }
  },
  /**
   * Get An investor
   * @param {object} req
   * @param {object} res
   * @returns {object} investor object
   */
  async getOne(req, res) {
    const text = "SELECT * FROM investors WHERE id = $1";
    try {
      const { rows } = await db.query(text, [req.params.id]);
      if (!rows[0]) {
        return res.status(404).send({ message: "Perfil investidor não encontrado" });
      }
      return res.status(200).send(rows[0]);
    } catch (error) {
      return res.status(400).send(error);
    }
  },
  /**
   * Update An investor
   * @param {object} req
   * @param {object} res
   * @returns {object} updated investor
   */
  async update(req, res) {
    const findOneQuery = "SELECT * FROM investors WHERE id=$1";
    const updateOneQuery = `UPDATE investors
    ,super_angel
      SET investor_name=$1,email=$2,city=$3,country=$4,balance=$5,photo=$6,modified_date=$7,user_id=$8,portfolio_value=$9,first_access=$10,super_angel=$11
      WHERE id=$12 returning *`;
    try {
      const { rows } = await db.query(findOneQuery, [req.params.id]);
      if (!rows[0]) {
        return res.status(404).send({ message: "investor not found" });
      }
      const values = [
        req.body.name || rows[0].name,,
        req.body.email || rows[0].email,
        req.body.city || rows[0].city,
        req.body.country || rows[0].country,,
        req.body.balance || rows[0].balance,,
        req.body.photo || rows[0].photo,,
        moment(new Date()),
        userLocal.id,
        req.body.balance,
        false,
        req.body.super_angel || rows[0].super_angel,
        req.params.id
      ];
      const response = await db.query(updateOneQuery, values);
      return res.status(200).send(response.rows[0]);
    } catch (err) {
      return res.status(400).send(err);
    }
  },
  /**
   * Delete An investor
   * @param {object} req
   * @param {object} res
   * @returns {void} return statuc code 204
   */
  async delete(req, res) {
    const deleteQuery = "DELETE FROM investors WHERE id=$1 returning *";
    try {
      const { rows } = await db.query(deleteQuery, [req.params.id]);
      if (!rows[0]) {
        return res.status(404).send({ message: "investor not found" });
      }
      return res.status(204).send({ message: "deleted" });
    } catch (error) {
      return res.status(400).send(error);
    }
  }
};

module.exports = Investor;
