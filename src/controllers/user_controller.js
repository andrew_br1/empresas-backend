const db = require("../db/index");
const moment = require("moment");
const helper = require("../controllers/helper");
const Investor = require("../controllers/investor_controller");
const User = {
  /**
   * Login
   * @param {object} req
   * @param {object} res
   * @returns {object} user object
   */
  async login(req, res) {
    if (!req.body.email || !req.body.password) {
      return res
        .status(400)
        .send({ message: "Email e/ou senha estão ausentes." });
    }
    if (!helper.isValidEmail(req.body.email)) {
      return res
        .status(400)
        .send({ message: "Por favor, insira um email válido." });
    }
    const text = "SELECT * FROM users WHERE email = $1";
    try {
      
      const { rows } = await db.query(text, [req.body.email]);
      if (!rows[0]) {
        return res
          .status(400)
          .send({ message: "Email e/ou senha estão incorretos." });
      }
      if (!helper.comparePassword(rows[0].password, req.body.password)) {
        return res
          .status(400)
          .send({ message: "Email e/ou senha estão incorretos." });
      }
      
      const token = helper.generateToken(rows[0].id);

      var result = await Investor.search(rows[0]);
      //console.log(result);

      const { user } = Object.create({ user: { investor: result } });

      if (user.investor.portfolio.enterprises_number === 0) {
        user.enterprise = null;
      }
      user.enterprise = true;
      user.success = true;
      const accessToken = helper.generateToken(rows[0].id);
      //console.log(req);
      res.writeHead(200, {
        "Content-Type": "application/json",
        dev_host: req.headers.host,
        "access-token": accessToken,
        client: accessToken,
        uid: rows[0].email
      });

     

      return res.write(JSON.stringify(user));
     
    } catch (error) {
      console.log(error);
      return res.status(400).send({ message: "Erro ao realizar o login." });
    }
  },
  /**
   * Create A user
   * @param {object} req
   * @param {object} res
   * @returns {object} user object
   */
  async create(req, res) {
    if (!req.body.email || !req.body.password) {
      return res
        .status(400)
        .send({ message: "Email e/ou senha estão ausentes." });
    }
    if (!helper.isValidEmail(req.body.email)) {
      return res
        .status(400)
        .send({ message: "Por favor, insira um email válido." });
    }
    //const hashPassword = helper.hashPassword(req.body.password);

    const text = `INSERT INTO
      users(email, password, created_date, modified_date)
      VALUES($1, $2, $3, $4)
      returning *`;
    const values = [
      req.body.email,
      req.body.password/* hashPassword */,
      moment(new Date()),
      moment(new Date())
    ];

    try {
      const { rows } = await db.query(text, values);
      const { investor } = await Investor.create(req, res, rows[0]);
      // console.log(investor);

      const { user } = Object.create({ user: { investor: investor } });
      user.enterprise = null;
      user.success = true;
      //const token = helper.generateToken(rows[0].id);
      //token;

      return res.status(201).send(user);
    } catch (error) {
      if (error.routine === "_bt_check_unique") {
        return res
          .status(400)
          .send({ message: "Email já está sendo utilizado." });
      }
      console.log(error);

      return res.status(400).send({ message: "Erro ao criar usuário." });
    }
  },
  /**
   * Get All user
   * @param {object} req
   * @param {object} res
   * @returns {object} users array
   */
  async getAll(req, res) {
    const findAllQuery = "SELECT * FROM users";
    try {
      const { rows, rowCount } = await db.query(findAllQuery);
      return res.status(200).send({ rows, rowCount });
    } catch (error) {
      return res.status(400).send(error);
    }
  },
  /**
   * Get A user
   * @param {object} req
   * @param {object} res
   * @returns {object} user object
   */
  async getOne(req, res) {
    const text = "SELECT * FROM users WHERE id = $1";
    try {
      const { rows } = await db.query(text, [req.params.id]);
      if (!rows[0]) {
        return res.status(404).send({ message: "Usuário não encontrado." });
      }
      return res.status(200).send(rows[0]);
    } catch (error) {
      return res.status(400).send(error);
    }
  },
  /**
   * Update A user
   * @param {object} req
   * @param {object} res
   * @returns {object} updated user
   */
  async update(req, res) {
    const findOneQuery = "SELECT * FROM users WHERE id=$1";
    const updateOneQuery = `UPDATE users
      SET email=$1,password=$2,modified_date=$3
      WHERE id=$4 returning *`;
    try {
      const { rows } = await db.query(findOneQuery, [req.params.id]);
      if (!rows[0]) {
        return res.status(404).send({ message: "Usuário não encontrado." });
      }
      const values = [
        req.body.name || rows[0].name,
        req.body.password || rows[0].password,
        moment(new Date()),
        req.params.id
      ];
      const response = await db.query(updateOneQuery, values);
      return res.status(200).send(response.rows[0]);
    } catch (err) {
      return res.status(400).send(err);
    }
  },
  /**
   * Delete A user
   * @param {object} req
   * @param {object} res
   * @returns {void} return statuc code 204
   */
  async delete(req, res) {
    const deleteQuery = "DELETE FROM users WHERE id=$1 returning *";
    try {
      const { rows } = await db.query(deleteQuery, [req.params.id]);
      if (!rows[0]) {
        return res.status(404).send({ message: "Usuário não encontrado." });
      }
      return res.status(204).send({ message: "Excluído com sucesso." });
    } catch (error) {
      return res.status(400).send(error);
    }
  }
};

module.exports = User;
