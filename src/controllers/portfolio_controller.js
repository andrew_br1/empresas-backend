const db = require("../db/index");
const moment = require("moment");
const Portfolio = {
  async search(investor) {
    const query2 = `SELECT id,name,investor_id,COUNT(*) as enterprises_number FROM portfolios where investor_id=$1 group by id;
      `;
    const value2 = [investor.id];

    const { rows } = await db.query(query2, value2);

    if (rows.length === 0) {
      const { portfolio } = Object.create({
        portfolio: {
          enterprises_number: 0,
          enterprises: []
        }
      });

      return portfolio;
    }
    return Object.create({
      portfolio: {
        enterprises_number: rows[0].enterprises_number,
        enterprises: rows
      }
    });
  },

  /**
   * Create A portfolio
   * @param {object} req
   * @param {object} res
   * @returns {object} portfolio object
   */
  async create(req, res) {
    const text = `INSERT INTO
      portfolios(
        investor_id ,
        name,
        created_date,
        deleted_date)
      VALUES($1, $2, $3, $4)
      returning *`;
    const values = [
      req.body.investor_id,
      req.body.name,
      moment(new Date()),
      null
    ];

    try {
      const { rows } = await db.query(text, values);
      return res.status(201).send(rows[0]);
    } catch (error) {
      return res.status(400).send(error);
    }
  },
  /**
   * Get All portfolio
   * @param {object} req
   * @param {object} res
   * @returns {object} portfolios array
   */
  async getAll(req, res) {
    const findAllQuery = "SELECT * FROM portfolios";
    try {
      const { rows, rowCount } = await db.query(findAllQuery);
      return res.status(200).send({ rows, rowCount });
    } catch (error) {
      return res.status(400).send(error);
    }
  },
  /**
   * Get A portfolio
   * @param {object} req
   * @param {object} res
   * @returns {object} portfolio object
   */
  async getOne(req, res) {
    const text = "SELECT * FROM portfolios WHERE id = $1";
    try {
      const { rows } = await db.query(text, [req.params.id]);
      if (!rows[0]) {
        return res.status(404).send({ message: "portfolio not found" });
      }
      return res.status(200).send(rows[0]);
    } catch (error) {
      return res.status(400).send(error);
    }
  },
  // NÃO CONFIGURADO PARA EDITAR. CRIAR E EDITAR, DEVIDO A EXCLUSÃO LÓGICA IMPLEMENTADA
  // /**
  //  * Update A portfolio
  //  * @param {object} req
  //  * @param {object} res
  //  * @returns {object} updated portfolio
  //  */
  // async update(req, res) {
  //   const findOneQuery = "SELECT * FROM portfolios WHERE id=$1";
  //   const updateOneQuery = `UPDATE portfolios
  //     SET success=$1,low_point=$2,take_away=$3,modified_date=$4
  //     WHERE id=$5 returning *`;
  //   try {
  //     const { rows } = await db.query(findOneQuery, [req.params.id]);
  //     if (!rows[0]) {
  //       return res.status(404).send({ message: "portfolio not found" });
  //     }
  //     const values = [
  //       req.body.success || rows[0].success,
  //       req.body.low_point || rows[0].low_point,
  //       req.body.take_away || rows[0].take_away,
  //       moment(new Date()),
  //       req.params.id
  //     ];
  //     const response = await db.query(updateOneQuery, values);
  //     return res.status(200).send(response.rows[0]);
  //   } catch (err) {
  //     return res.status(400).send(err);
  //   }
  // },
  /**
   * Delete A portfolio
   * @param {object} req
   * @param {object} res
   * @returns {void} return statuc code 204
   */
  async delete(req, res) {
    const deleteQuery = "INSERT INTO portfolios(deleted_date) VALUES($1) WHERE id=$2 returning *";
    const values1 = [
      moment(new Date()),
      req.params.id
    ]
    try {
      const { rows } = await db.query(deleteQuery, values1);
      if (!rows[0]) {
        return res.status(404).send({ message: "Portfolio não enconjtrado." });
      }
      return res.status(204).send({ message: "Excluidos com sucesso." });
    } catch (error) {
      return res.status(400).send(error);
    }
  }
};

module.exports = Portfolio;
